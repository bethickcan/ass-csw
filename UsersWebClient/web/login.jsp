<%-- 
    Document   : login
    Created on : Oct 3, 2019, 12:20:11 PM
    Author     : tienh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Login</h1>
        <form action="LoginServlet" >
            <div class="main">
                <h1><span>Admin</span> <label> Login</label></h1>
                <div class="inset">
                    <p>
                        <label for="email">Username</label>
                        <input type="text" name="username" placeholder="" required />
                    </p>
                    <p>
                        <label for="password">Password</label>
                        <input type="password" name="password" placeholder="" required />
                    </p>
                    <p>
                        <input type="checkbox" name="remember" id="remember" />
                        <label for="remember">Remember me</label>
                    </p>
                </div>
                <p class="p-container">
                    <span><a href="#">Forgot password?</a></span>
                    <input type="submit" value="Login"/>
                </p>
            </div>
        </form>
        <div class="copy-right">
            <p>Design by <a href="#">fpt-aptech</a></p>
        </div>
    </body>
</html>
